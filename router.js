const router = require('express').Router();
const testController = require('./Controller/hello');

router.get('/', testController.get);
router.post('/api/v1/posts', testController.create);
router.get('/api/v1/posts', testController.getAll);
router.put('/api/v1/posts/:id', testController.update);
router.delete('/api/v1/posts/:id', testController.delete);
module.exports = router;