'use strict';
module.exports = (sequelize, DataTypes) => {
  const post = sequelize.define('post', {
    title: {
      type: DataTypes.STRING,
      allowNull: false
    },
    body: DataTypes.TEXT
  }, {});
  post.associate = function(models) {
    // associations can be defined here
  };
  return post;
};