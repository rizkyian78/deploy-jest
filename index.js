const express = require('express');
const app = express();
const router = require('./router');
require('dotenv').config();

app.use(express.json());

app.use(router);

module.exports = app;