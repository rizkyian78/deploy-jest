const request = require('supertest');
const app = require('../index');

describe('ROOT APi', () => {
    describe('GET /', () => {
        test('should return 200', done => {
            request(app).get('/')
            .then(res => {
                expect(res.body.status).toEqual('Hello World');
                // Done is Important
                done();
            })
        })
        
    })
})

